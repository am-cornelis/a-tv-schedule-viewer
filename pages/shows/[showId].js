import Head from 'next/head'
import Link from 'next/link'
import Image from 'next/image'
import Layout from '../../components/layout'
import utilStyles from '../../styles/utils.module.css'
import { getShowDetails, getImageUrl } from '../../lib/show-provider'

const stripHTMLTags = (string) => string.replace(/(<([^>]+)>)/gi, "");

export async function getServerSideProps(context) {
  const showDetails = await getShowDetails(context.query.showId);
  return {
    props: {
      showDetails,
    }
  }
}

export default function AShow({ showDetails }) {
  return (
    <Layout>
      <Head>
        <title>Show Details</title>
      </Head>
      <section className={`${utilStyles.headingMd}`}>
        <Image
          priority
          src={ getImageUrl(showDetails) }
          height={144}
          width={144}
          alt="picture of the show"
        />
        <p>{ showDetails.rating.average || '' }</p>
        <h2 className={utilStyles.headingLg}>{showDetails.name}</h2>
        <p>{ stripHTMLTags(showDetails.summary) }</p>
      </section>
      <section className={`${utilStyles.headingMd}`}>
        <h3 className={utilStyles.headingLg}>Show Info</h3>
        <ul>
            <li>Streamed on: {showDetails.network.name}</li>
            <li>Schedule: {showDetails.schedule.days.join(",")}</li>
            <li>Status: {showDetails.status}</li>
            <li>Genres: {showDetails.genres.join(",")}</li>
        </ul>
      </section>
      <section className={`${utilStyles.headingMd}`}>
        <h3 className={utilStyles.headingLg}>Starring</h3>
        <ul>
          {showDetails._embedded.cast.map(({ person, character }) => (
            <li key={ person.id }>
                <Image
                  priority
                  src={ getImageUrl(person) }
                  height={25}
                  width={25}
                  alt="picture of the person"
                />
                {'   '}
                {person.name} - {character.name}
                {'   '}
                <Image
                  priority
                  src={ getImageUrl(character) }
                  height={25}
                  width={25}
                  alt="picture of the character"
                />
            </li>
          ))}
        </ul>
      </section>
    </Layout>
  )
}
