import Head from 'next/head'
import Link from 'next/link'
import Image from 'next/image'
import Layout, { siteTitle } from '../components/layout'
import utilStyles from '../styles/utils.module.css'
import { getShows, getImageUrl } from '../lib/show-provider'

export async function getStaticProps() {
  const allShowsData = await getShows();
  return {
    props: {
      allShowsData
    }
  }
}

export default function Home({ allShowsData }) {
  return (
    <Layout home>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <section className={utilStyles.headingMd}>
        <p>TV Show and web series database.</p>
        <p>Create personalised schedules. Get episode guide, cast, crew and character information.</p>
      </section>
      <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
        <h2 className={utilStyles.headingLg}>Last Added Shows</h2>
        <ul className={utilStyles.list}>
          {allShowsData.map(({ show }) => (
            <li className={utilStyles.listItem} key={show.id}>
              <Link href={`/shows/${show.id}`}>
                <a>
                    <Image
                      priority
                      src={ getImageUrl(show) }
                      height={25}
                      width={25}
                      alt="picture of the show"
                    />
                    {'   '}
                    {show.name}
                    </a>
              </Link>
            </li>
          ))}
          { allShowsData.length === 0 && <li>No shows available, please try again later.</li> }
        </ul>
      </section>
    </Layout>
  )
}
