# Exercise: TV Schedule

This is an exploration of the [Next.js](https://nextjs.org) framework, following [this tutorial](https://nextjs.org/learn/basics/create-nextjs-app), and consuming the [TV Maze api](https://www.tvmaze.com/api#schedule) instead.

As it is pretty obvious with this codebase, front-end in general is an area where I have much to learn.

## To run

Assuming Node is already installed:
```
npm install
npm run dev
```

This was developed/tested with Node 10 which is rather old, but:
* happens to be the latest available version for WSL/Ubuntu, which I chose for doing this exercise
* is - just about - [maintained](https://nodejs.org/en/about/releases/) at the time of writing, March 2021

## TODO

Would to next: get closer to the desired wireframes, i.e.
* Research the right components for responsive design
* Better markup on the show details page, so it's possible to style it
* Sort out the overlaping grey background at the top/bottom
* Work out how to display the star rating
  * (also, find out out of how many? There are examples in the data of 6.6 so it doesn't look like it's out of 5)

## Notes

The data is hard-coded for GB, as this is currently where I live. There is probably a way to detect a visitor's LOCALE instead.

I had a quick look at other regions of the world and the data seems very sparce outside of the US/GB, so I'd look for additional sources of data.

It currently uses a fixed date (2021-03-24) because I haven't yet put good checks when displaying the fields of the show details page, and the data quality varies from day to day. I fixed the fields where it broke today (2021-03-25), but there might be things that break tomorrow.
