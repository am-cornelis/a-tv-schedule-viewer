const getJsonOrDefault = async (url, defaultValue) => {
  return await fetch(url)
  .then((response) => {
    if (response.status >= 200 && response.status <= 299) {
      return response.json();
    } else {
      console.log(`Could not fetch ${url}`, response.status, response.statusText);
      return defaultValue;
    }
  })
  .catch((error) => {
      console.log(`Could not fetch ${url}`, error);
      return defaultValue;
  });
}

export const getShows = async () => {
    return await getJsonOrDefault('https://api.tvmaze.com/schedule?country=GB&date=2021-03-24', []);
}

export const getShowDetails = async (showId) => {
    return await getJsonOrDefault(`https://api.tvmaze.com/shows/${showId}?embed=cast`, {});
}

export const getImageUrl = (showOrCast) => {
    const imageUrl = showOrCast?.image?.medium;
    if(imageUrl) return imageUrl;
    return '/images/show_icon.png';
}
